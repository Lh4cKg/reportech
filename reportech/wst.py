# _*_ coding: utf-8 _*_

"""
Created on Jan 12, 2018

@author: Lasha Gogua
"""

import json
from collections import OrderedDict
import requests

# app import
from . import config
from .utils import parse_html, check_app


class DetectionWST(object):
    """
        DetectionWST - Detection the website technologies

    """

    apps_db = getattr(config, "APPS", None)  # default apps database
    cats = getattr(config, "CATEGORIES", None)  # default categories

    def __init__(self, url, content=None, headers=None, verify=False, timeout=None, meta=None, scripts=None, apps=None, categories=None):
        """

        :param url: type of str
                website URL
        :param content: type of str
                website content
        :param headers: type of str
                website response headers
        :param verify: type of bool
                website SSL request verification, default is False
        :param timeout: type of int or float
                website connection timeout, wait for the server to send data, default value is 5 second
        :param meta: type of list
                website <meta> tags
        :param scripts: type of list
                website <script> tags
        :param apps: type of dict
                web applications database
        :param categories: type of dict
                web technology categories

        """

        self.url = url
        self.content = content
        self.headers = headers
        self.verify = verify
        self.timeout = timeout if timeout is None else 5  # second
        self._meta = meta
        self._scripts = scripts
        self._apps = apps
        self._categories = categories

        if self.headers is None:
            self.headers = {}

        if isinstance(self.headers, dict) is False:
            raise ValueError("""HTTP headers allow the client and the server to pass additional information with the 
            request or the response. A request header consists of its case-insensitive name followed by a colon ':', 
            then by its value (without line breaks). Leading white space before the value is ignored.""")

        self.response = self._response()

    @property
    def apps(self):
        """
        web applications database
        using a apps database

        :return: apps

        """

        if self._apps:
            return self._apps

        return self.apps_db

    @property
    def categories(self):
        """
        web technology categories
        using a categories db

        :return: categories

        """

        if self._categories:
            return self._categories

        return self.cats

    @property
    def meta(self):
        """
        Parse the HTML with lxml to find <meta> tags.
        :return: meta

        """

        if self._meta:
            return self._meta

        meta = parse_html(self.content, "meta")
        return meta

    @property
    def scripts(self):
        """
        Parse the HTML with lxml to find <script> tags.
        :return: scripts

        """

        if self._scripts:
            return self._scripts

        scripts = parse_html(self.content, "script")
        return scripts

    def _response(self):
        """
        HTTP Response - using the `requests` module to fetch the HTML.
        :return: response

        """

        response = requests.get(self.url, verify=self.verify, timeout=self.timeout)

        if not self.content:
            self.content = response.content.decode('utf-8')
        if not self.headers:
            self.headers = response.headers

        return response

    def screening(self):
        """

        :return: result
        """

        web_server_info = self.headers.get("Server").split("/")
        result = OrderedDict({
            "Website": self.url,
            "Web Server": web_server_info[0] if web_server_info.__len__() > 0 else "",
            "Web Server Version": web_server_info[1] if web_server_info.__len__() > 1 else "",
        })

        for name, app in self.apps.items():
            if check_app(app=app, content=self.content, headers=self.headers, meta=self.meta, scripts=self.scripts):
                result.update({
                    name: {
                        "Category": [self.categories.get(cat) for cat in app.get("cats")],
                        "Application": name,
                        "Application Version": "",
                        "Programming Language": app.get("implies"),
                        "Programming Language Version": ""
                    }
                })

        return json.dumps(result, indent=4)
