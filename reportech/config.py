# _*_ coding: utf-8 _*_

"""
Created on Jan 12, 2018

@author: Lasha Gogua
"""

from collections import OrderedDict


APPS = OrderedDict({
    "Python": {
        "cats": [
            27
        ],
        "headers": {
            "Server": "(?:^|\\s)Python(?:/([\\d.]+))?\\;confidence:50\\;version:\\1"
        },
        "icon": "Python.png",
        "website": "python.org"
    },
    "PHP": {
        "cats": [
            27
        ],
        "headers": {
            "Server": "php/?([\\d.]+)?\\;confidence:40\\;version:\\1",
            "Set-Cookie": "PHPSESSID",
            "X-Powered-By": "php/?([\\d.]+)?\\;confidence:40\\;version:\\1"
        },
        "icon": "PHP.png",
        "url": "\\.php(?:$|\\?)",
        "website": "php.net"
    },
    "Java": {
        "cats": [
            27
        ],
        "headers": {
            "Set-Cookie": "JSESSIONID"
        },
        "icon": "Java.png",
        "website": "java.com"
    },
    "Microsoft ASP.NET": {
        "cats": [
            18
        ],
        "headers": {
            "Set-Cookie": "ASPSESSION|ASP\\.NET_SessionId",
            "X-AspNet-Version": "(.+)\\;version:\\1",
            "X-Powered-By": "ASP\\.NET\\;confidence:50"
        },
        "html": "<input[^>]+name=\"__VIEWSTATE",
        "icon": "Microsoft ASP.NET.png",
        "implies": "IIS\\;confidence:50",
        "url": "\\.aspx(?:$|\\?)",
        "website": "www.asp.net"
    },
    "Django": {
        "cats": [
            18
        ],
        "env": "^__admin_media_prefix__",
        "html": "(?:powered by <a[^>]+>Django ?([\\d.]+)?|<input[^>]*name=[\"']csrfmiddlewaretoken[\"'][^>]*>)\\;version:\\1",
        "icon": "Django.png",
        "implies": "Python",
        "website": "djangoproject.com"
    },
    "Flask": {
        "cats": [
            18,
            22
        ],
        "headers": {
            "Server": "Werkzeug/?([\\d\\.]+)?\\;version:\\1"
        },
        "icon": "Flask.png",
        "implies": "Python",
        "website": "flask.pocoo.org"
    },
    "Laravel": {
        "cats": [
            18
        ],
        "headers": {
            "Set-Cookie": "laravel_session"
        },
        "icon": "Laravel.png",
        "implies": "PHP",
        "website": "laravel.com"
    },
    "CodeIgniter": {
        "cats": [
            18
        ],
        "headers": {
            "Set-Cookie": "(?:exp_last_activity|exp_tracker|ci_(?:session|(csrf_token)))\\;version:\\1?2+:"
        },
        "html": "<input[^>]+name=\"ci_csrf_token\"\\;version:2+",
        "icon": "CodeIgniter.png",
        "implies": "PHP",
        "website": "codeigniter.com"
    },
    "Zend": {
        "cats": [
            22
        ],
        "headers": {
            "Set-Cookie": "ZENDSERVERSESSID",
            "X-Powered-By": "Zend"
        },
        "icon": "Zend.png",
        "website": "zend.com"
    },
    "Yii": {
        "cats": [
            18
        ],
        "html": [
            "Powered by <a href=\"http://www.yiiframework.com/\" rel=\"external\">Yii Framework</a>"
        ],
        "icon": "Yii.png",
        "implies": [
            "PHP"
        ],
        "website": "yiiframework.com"
    },
    "Symfony": {
        "cats": [
            18
        ],
        "icon": "Symfony.png",
        "implies": "PHP",
        "website": "symfony.com"
    },
    "CakePHP": {
        "cats": [
            18
        ],
        "headers": {
            "Set-Cookie": "cakephp="
        },
        "icon": "CakePHP.png",
        "implies": "PHP",
        "meta": {
            "application-name": "CakePHP"
        },
        "website": "cakephp.org"
    },
    "Twitter Bootstrap": {
        "cats": [
            18
        ],
        "env": "^Twipsy$\\;confidence:50",
        "html": [
            "<style>/\\*!\\* Bootstrap v(\\d\\.\\d\\.\\d)\\;version:\\1",
            "<link[^>]+?href=\"[^\"]+bootstrap(?:\\.min)?\\.css",
            "<div [^>]*class=\"[^\"]*col-(?:xs|sm|md|lg)-\\d{1,2}"
        ],
        "icon": "Twitter Bootstrap.png",
        "script": "(?:twitter\\.github\\.com/bootstrap|bootstrap(?:\\.js|\\.min\\.js))",
        "website": "getbootstrap.com"
    },
    "Joomla": {
        "cats": [
            1
        ],
        "env": "^(?:jcomments|Joomla)$",
        "headers": {
            "X-Content-Encoded-By": "Joomla! ([\\d.]+)\\;version:\\1"
        },
        "html": "(?:<div[^>]+id=\"wrapper_r\"|<[^>]+(?:feed|components)/com_|<table[^>]+class=\"pill)\\;confidence:50",
        "icon": "Joomla.png",
        "implies": "PHP",
        "meta": {
            "generator": "Joomla!(?: ([\\d.]+))?\\;version:\\1"
        },
        "url": "option=com_",
        "website": "joomla.org"
    },
    "Drupal": {
        "cats": [
            1
        ],
        "env": "^Drupal$",
        "headers": {
            "Expires": "19 Nov 1978",
            "X-Drupal-Cache": "",
            "X-Generator": "Drupal(?:\\s([\\d.]+))?\\;version:\\1"
        },
        "html": "<(?:link|style)[^>]+sites/(?:default|all)/(?:themes|modules)/",
        "icon": "Drupal.png",
        "implies": "PHP",
        "meta": {
            "generator": "Drupal(?:\\s([\\d.]+))?\\;version:\\1"
        },
        "script": "drupal\\.js",
        "website": "drupal.org"
    },
    "Drupal Commerce": {
        "cats": [
            6
        ],
        "html": "<[^>]+(?:id=\"block[_-]commerce[_-]cart[_-]cart|class=\"commerce[_-]product[_-]field)",
        "icon": "Drupal Commerce.png",
        "implies": "Drupal",
        "website": "drupalcommerce.org"
    },
    "Fedora": {
        "cats": [
            28
        ],
        "headers": {
            "Server": "Fedora"
        },
        "icon": "Fedora.png",
        "website": "fedoraproject.org"
    },
    "Red Hat": {
        "cats": [
            28
        ],
        "headers": {
            "Server": "Red Hat",
            "X-Powered-By": "Red Hat"
        },
        "icon": "Red Hat.png",
        "website": "redhat.com"
    },
    "CentOS": {
        "cats": [
            28
        ],
        "headers": {
            "Server": "CentOS",
            "X-Powered-By": "CentOS"
        },
        "icon": "CentOS.png",
        "website": "centos.org"
    },
    "UNIX": {
        "cats": [
            28
        ],
        "headers": {
            "Server": "Unix"
        },
        "icon": "UNIX.png",
        "website": "unix.org"
    },
    "Ubuntu": {
        "cats": [
            28
        ],
        "headers": {
            "Server": "Ubuntu",
            "X-Powered-By": "Ubuntu"
        },
        "icon": "Ubuntu.png",
        "website": "www.ubuntu.com/server"
    },
    "Debian": {
        "cats": [
            28
        ],
        "headers": {
            "Server": "Debian",
            "X-Powered-By": "(?:Debian|dotdeb|(sarge|etch|lenny|squeeze|wheezy|jessie))\\;version:\\1"
        },
        "icon": "Debian.png",
        "website": "debian.org"
    },
    "IIS": {
        "cats": [
            22
        ],
        "headers": {
            "Server": "IIS(?:/([\\d.]+))?\\;version:\\1"
        },
        "icon": "IIS.png",
        "implies": "Windows Server",
        "website": "www.iis.net"
    },
    "Windows Server": {
        "cats": [
            28
        ],
        "headers": {
            "Server": "Win32|Win64"
        },
        "icon": "Microsoft.svg",
        "website": "microsoft.com/windowsserver"
    },
    "Vue.js": {
        "cats": [
            12
        ],
        "env": "^Vue$",
        "icon": "Vue.js.png",
        "script": [
            "vue(?:\\-|\\.)([\\d.]*\\d)[^/]*\\.js\\;version:\\1",
            "/([\\d.]+)/vue(?:\\.min)?\\.js\\;version:\\1",
            "vue.*\\.js\\;confidence:20"
        ],
        "website": "vuejs.org"
    },
    "AngularJS": {
        "cats": [
            12
        ],
        "env": "^angular$",
        "icon": "AngularJS.png",
        "script": [
            "angular(?:\\-|\\.)([\\d.]*\\d)[^/]*\\.js\\;version:\\1",
            "/([\\d.]+(?:\\-?rc[.\\d]*)*)/angular(?:\\.min)?\\.js\\;version:\\1",
            "angular.*\\.js"
        ],
        "website": "angularjs.org"
    },
    "Ember.js": {
        "cats": [
            12
        ],
        "env": "^Ember$",
        "icon": "Ember.js.png",
        "implies": "Handlebars",
        "website": "emberjs.com"
    },
    "Apache": {
        "cats": [
            22
        ],
        "headers": {
            "Server": "(?:Apache(?:$|/([\\d.]+)|[^/-])|(?:^|\b)HTTPD)\\;version:\\1"
        },
        "icon": "Apache.svg",
        "website": "apache.org"
    },
    "Nginx": {
        "cats": [
            22
        ],
        "headers": {
            "Server": "nginx(?:/([\\d.]+))?\\;version:\\1"
        },
        "icon": "Nginx.svg",
        "website": "nginx.org"
    },
    "Node.js": {
        "cats": [
            27
        ],
        "icon": "node.js.png",
        "website": "nodejs.org"
    },
    "ExtJS": {
        "cats": [
            12
        ],
        "env": "^Ext$",
        "icon": "ExtJS.png",
        "script": "ext-base\\.js",
        "website": "www.extjs.com"
    },
    "React": {
        "cats": [
            12
        ],
        "env": "^React$",
        "html": "<[^>]+data-react",
        "icon": "React.png",
        "script": [
            "react(?:\\-with\\-addons)?(?:\\-|\\.)([\\d.]*\\d)[^/]*\\.js\\;version:\\1",
            "/([\\d.]+)/react(?:\\.min)?\\.js\\;version:\\1",
            "react.*\\.js"
        ],
        "website": "facebook.github.io/react"
    },
    "OWL Carousel": {
        "cats": [
            5,
            7
        ],
        "html": "<link [^>]*href=\"[^\"]+owl.carousel(?:\\.min)?\\.css",
        "icon": "OWL Carousel.png",
        "implies": "jQuery",
        "script": "owl.carousel.*\\.js",
        "website": "owlgraphic.com/owlcarousel"
    },
    "Select2": {
        "cats": [
            12
        ],
        "icon": "default.png",
        "implies": "jQuery",
        "script": "select2.*\\.js",
        "website": "select2.github.io"
    },
    "Materialize CSS": {
        "cats": [
            18
        ],
        "html": "<link[^>]* href=\"[^\"]*materialize(?:\\.min)?\\.css",
        "icon": "Materialize CSS.png",
        "implies": "jQuery",
        "script": "materialize(?:\\.min)?\\.js",
        "website": "materializecss.com"
    },
    "prettyPhoto": {
        "cats": [
            7,
            12
        ],
        "env": "pp_(?:alreadyInitialized|descriptions|images|titles)",
        "html": "(?:<link [^>]*href=\"[^\"]*prettyPhoto(?:\\.min)?\\.css|<a [^>]*rel=\"prettyPhoto)",
        "icon": "prettyPhoto.png",
        "implies": "jQuery",
        "script": "jquery\\.prettyPhoto\\.js",
        "website": "no-margin-for-errors.com/projects/prettyphoto-jquery-lightbox-clone/"
    },
    "jQuery": {
        "cats": [
            12
        ],
        "env": "^jQuery$",
        "icon": "jQuery.svg",
        "script": [
            "jquery(?:\\-|\\.)([\\d.]*\\d)[^/]*\\.js\\;version:\\1",
            "/([\\d.]+)/jquery(?:\\.min)?\\.js\\;version:\\1",
            "jquery.*\\.js"
        ],
        "website": "jquery.com"
    },
    "jQuery Mobile": {
        "cats": [
            26
        ],
        "icon": "jQuery Mobile.svg",
        "implies": "jQuery",
        "script": "jquery\\.mobile(?:-([\\d.]+rc\\d))?.*\\.js(?:\\?ver=([\\d.]+))?\\;version:\\1?\\1:\\2",
        "website": "jquerymobile.com"
    },
    "jQuery Sparklines": {
        "cats": [
            25
        ],
        "icon": "default.png",
        "implies": "jQuery",
        "script": "jquery\\.sparkline.*\\.js",
        "website": "omnipotent.net/jquery.sparkline/"
    },
    "jQuery UI": {
        "cats": [
            12
        ],
        "icon": "jQuery UI.svg",
        "implies": "jQuery",
        "script": [
            "jquery-ui(?:-|\\.)([\\d.]*\\d)[^/]*\\.js\\;version:\\1",
            "([\\d.]+)/jquery-ui(?:\\.min)?\\.js\\;version:\\1",
            "jquery-ui.*\\.js"
        ],
        "website": "jqueryui.com"
    },
    "Express": {
        "cats": [
            18,
            22
        ],
        "headers": {
            "X-Powered-By": "^Express$"
        },
        "icon": "Express.png",
        "implies": "Node.js",
        "website": "expressjs.com"
    },
    "Angular Material": {
        "cats": [
            18
        ],
        "env": "^angular$",
        "icon": "Angular.svg",
        "implies": [
            "AngularJS"
        ],
        "script": [
            "/([\\d.]+(?:\\-?rc[.\\d]*)*)/angular-material(?:\\.min)?\\.js\\;version:\\1",
            "angular-material.*\\.js"
        ],
        "website": "material.angularjs.org"
    },
    "Apache HBase": {
        "cats": [
            34
        ],
        "html": "<style[^>]+static/hbase",
        "icon": "Apache HBase.png",
        "website": "hbase.apache.org"
    },
    "Apache Hadoop": {
        "cats": [
            34
        ],
        "html": "<style[^>]+static/hadoop",
        "icon": "Apache Hadoop.png",
        "website": "hadoop.apache.org"
    },
    "Apache JSPWiki": {
        "cats": [
            8
        ],
        "html": "<html[^>]* xmlns:jspwiki=",
        "icon": "Apache JSPWiki.png",
        "script": "jspwiki",
        "url": "wiki\\.jsp",
        "website": "jspwiki.org"
    },
    "Apache Tomcat": {
        "cats": [
            22
        ],
        "headers": {
            "Server": "Apache-Coyote(/1\\.1)?\\;version:\\1?4.1+:",
            "X-Powered-By": "\bTomcat\b(?:-([\\d.]+))?\\;version:\\1"
        },
        "icon": "Apache Tomcat.png",
        "website": "tomcat.apache.org"
    },
    "Apache Traffic Server": {
        "cats": [
            22
        ],
        "headers": {
            "Server": "ATS/?([\\d.]+)?\\;version:\\1"
        },
        "icon": "Apache Traffic Server.png",
        "website": "trafficserver.apache.org/"
    },
    "Apache Wicket": {
        "cats": [
            18
        ],
        "env": "^Wicket",
        "icon": "Apache Wicket.png",
        "implies": "Java",
        "website": "wicket.apache.org"
    },
    "Backbone.js": {
        "cats": [
            12
        ],
        "env": "^Backbone$",
        "icon": "Backbone.js.png",
        "implies": "Underscore.js",
        "script": "backbone.*\\.js",
        "website": "backbonejs.org"
    },
    "CKEditor": {
        "cats": [
            24
        ],
        "env": "^CKEDITOR$",
        "icon": "CKEditor.png",
        "website": "ckeditor.com"
    },
    "CS Cart": {
        "cats": [
            6
        ],
        "env": "^fn_compare_strings$",
        "html": [
            "&nbsp;Powered by (?:<a href=[^>]+cs-cart\\.com|CS-Cart)",
            ".cm-noscript[^>]+</style>"
        ],
        "icon": "CS Cart.png",
        "implies": "PHP",
        "website": "www.cs-cart.com"
    },
    "OpenCart": {
        "cats": [
            6
        ],
        "html": "(?:index\\.php\\?route=[a-z]+/|Powered By <a href=\"[^>]+OpenCart)",
        "icon": "OpenCart.png",
        "implies": "PHP",
        "website": "www.opencart.com"
    },
    "PrestaShop": {
        "cats": [
            6
        ],
        "env": [
            "^freeProductTranslation$\\;confidence:25",
            "^freeProductTranslation$\\;confidence:25",
            "^priceDisplayMethod$\\;confidence:25",
            "^priceDisplayPrecision$\\;confidence:25"
        ],
        "html": "Powered by <a\\s+[^>]+>PrestaShop",
        "icon": "PrestaShop.png",
        "implies": "PHP",
        "meta": {
            "generator": "PrestaShop"
        },
        "website": "www.prestashop.com"
    },
    "phpBB": {
        "cats": [
            2
        ],
        "env": "^(?:style_cookie_settings|phpbb_)",
        "headers": {
            "Set-Cookie": "^phpbb"
        },
        "html": "(?:Powered by <a[^>]+phpbb|<a[^>]+phpbb[^>]+class=\\.copyright|\tphpBB style name|<[^>]+styles/(?:sub|pro)silver/theme|<img[^>]+i_icon_mini|<table class=\"forumline)",
        "icon": "phpBB.png",
        "implies": "PHP",
        "meta": {
            "copyright": "phpBB Group"
        },
        "website": "phpbb.com"
    },
    "phpCMS": {
        "cats": [
            1
        ],
        "env": "^phpcms",
        "icon": "phpCMS.png",
        "implies": "PHP",
        "website": "phpcms.de"
    },
    "Google AdSense": {
        "cats": [
            36
        ],
        "env": [
            "^google_ad_",
            "^__google_ad_",
            "^Goog_AdSense_"
        ],
        "icon": "Google AdSense.svg",
        "script": [
            "googlesyndication\\.com/",
            "ad\\.ca\\.doubleclick\\.net",
            "2mdn\\.net",
            "ad\\.ca\\.doubleclick\\.net"
        ],
        "website": "google.com/adsense"
    },
    "Google Analytics": {
        "cats": [
            10
        ],
        "env": "^gaGlobal$",
        "headers": {
            "Set-Cookie": "__utma"
        },
        "icon": "Google Analytics.svg",
        "script": "^https?://[^\\/]+\\.google-analytics\\.com\\/(?:ga|urchin|(analytics))\\.js\\;version:\\1?UA:",
        "website": "google.com/analytics"
    },
    "Google App Engine": {
        "cats": [
            22
        ],
        "headers": {
            "Server": "Google Frontend"
        },
        "icon": "Google App Engine.png",
        "website": "code.google.com/appengine"
    },
    "Google Charts": {
        "cats": [
            25
        ],
        "env": "^__g(?:oogleVisualizationAbstractRendererElementsCount|vizguard)__$",
        "icon": "Google Charts.png",
        "website": "developers.google.com/chart/"
    },
    "Google Code Prettify": {
        "cats": [
            19
        ],
        "env": "^prettyPrint$",
        "icon": "Google Code Prettify.png",
        "website": "code.google.com/p/google-code-prettify"
    },
    "Google Font API": {
        "cats": [
            17
        ],
        "env": "^WebFonts$",
        "html": "<link[^>]* href=[^>]+fonts\\.(?:googleapis|google)\\.com",
        "icon": "Google Font API.png",
        "script": "googleapis\\.com/.+webfont",
        "website": "google.com/fonts"
    },
    "Google Maps": {
        "cats": [
            35
        ],
        "icon": "Google Maps.png",
        "script": [
            "(?:maps\\.google\\.com/maps\\?file=api(?:&v=([\\d.]+))?|maps\\.google\\.com/maps/api/staticmap)\\;version:API v\\1",
            "//maps.googleapis.com/maps/api/js"
        ],
        "website": "maps.google.com"
    },
    "Google PageSpeed": {
        "cats": [
            23,
            33
        ],
        "headers": {
            "X-Mod-Pagespeed": "([\\d.]+)\\;version:\\1",
            "X-Page-Speed": "(.+)\\;version:\\1"
        },
        "icon": "Google PageSpeed.png",
        "website": "developers.google.com/speed/pagespeed/mod"
    },
    "Google Sites": {
        "cats": [
            1
        ],
        "icon": "Google Sites.png",
        "url": "sites\\.google\\.com",
        "website": "sites.google.com"
    },
    "Google Tag Manager": {
        "cats": [
            42
        ],
        "env": "^googletag$",
        "html": "googletagmanager\\.com/ns\\.html[^>]+></iframe>",
        "icon": "Google Tag Manager.png",
        "website": "www.google.com/tagmanager"
    },
    "Google Wallet": {
        "cats": [
            41
        ],
        "icon": "Google Wallet.png",
        "script": [
            "checkout\\.google\\.com",
            "wallet\\.google\\.com"
        ],
        "website": "wallet.google.com"
    },
    "Google Web Server": {
        "cats": [
            22
        ],
        "headers": {
            "Server": "gws"
        },
        "icon": "Google Web Server.png",
        "website": "en.wikipedia.org/wiki/Google_Web_Server"
    },
    "Google Web Toolkit": {
        "cats": [
            18
        ],
        "env": "^__gwt_",
        "icon": "Google Web Toolkit.png",
        "implies": "Java",
        "meta": {
            "gwt:property": ""
        },
        "website": "developers.google.com/web-toolkit"
    },
})


CATEGORIES = OrderedDict({
    1: "CMS",
    2: "Message Boards",
    3: "Database Managers",
    4: "Documentation Tools",
    5: "Widgets",
    6: "Ecommerce",
    7: "Photo Galleries",
    8: "Wikis",
    9: "Hosting Panels",
    10: "Analytics",
    11: "Blogs",
    12: "Javascript Frameworks",
    13: "Issue Trackers",
    14: "Video Players",
    15: "Comment Systems",
    16: "Captchas",
    17: "Font Scripts",
    18: "Web Frameworks",
    19: "Miscellaneous",
    20: "Editors",
    21: "LMS",
    22: "Web Servers",
    23: "Cache Tools",
    24: "Rich Text Editors",
    25: "Javascript Graphics",
    26: "Mobile Frameworks",
    27: "Programming Languages",
    28: "Operating Systems",
    29: "Search Engines",
    30: "Web Mail",
    31: "CDN",
    32: "Marketing Automation",
    33: "Web Server Extensions",
    34: "Databases",
    35: "Maps",
    36: "Advertising Networks",
    37: "Network Devices",
    38: "Media Servers",
    39: "Webcams",
    40: "Printers",
    41: "Payment Processors",
    42: "Tag Managers",
    43: "Paywalls",
    44: "Build CI Systems",
    45: "Control Systems",
    46: "Remote Access",
    47: "Dev Tools",
    48: "Network Storage",
    49: "Feed Readers",
    50: "Document Management Systems",
    51: "Landing Page Builders",
    52: "Live Chat",
})
