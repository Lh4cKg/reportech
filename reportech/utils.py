# _*_ coding: utf-8 _*_

"""
Created on Jan 12, 2018

@author: Lasha Gogua
"""

import re
from lxml import html


def parse_html(content, tag):
    """

    Parse the HTML content
    :param content: type of str or byte, website html as content
    :param tag: type of str, html tag

    :return: element
    """

    element_values = [] if tag != "meta" else {}
    tree = html.fromstring(content)
    elements = tree.xpath('//%s' % tag)
    for element in elements:
        for key, value in element.items():
            if tag == "meta" and key == "name":
                content_value = None
                for k, v in element.items():
                    if k != key:
                        content_value = element.get(k)
                element_values[element.get(key)] = content_value
                break
            if key == "src":
                element_values.append(element.get(key))

    return element_values


def regex_pattern(pattern):
    """
    pattern and compile the regular expression.
    :return: regex

    """

    regex = pattern.partition("\\;")

    return re.compile(regex[0], re.I)


def check_app(app, content, headers, meta, scripts):
    """

    :param app: application
    :param content: type of str, url, headers, script, meta or html
    :param headers: type of str, url, headers, script, meta or html
    :param meta: type of str, url, headers, script, meta or html
    :param scripts: type of str, url, headers, script, meta or html
    :return: True or False

    """

    # tags = ["url", "html", "script", "meta"]
    detected_app = False
    for key, value in app.items():
        if detected_app:
            return True
        if key == "url":
            if regex_pattern(app.get(key)).search(content):
                detected_app = True
        if key == "headers":
            [app.get(key).update({k: regex_pattern(v)}) for k, v in app.get(key).items()]
            for name, regex_value in app.get(key).items():
                if name in headers:
                    content = headers.get(name)
                    if regex_value.search(content):
                        detected_app = True
        if key == "meta":
            [app.get(key).update({k: regex_pattern(v)}) for k, v in app.get(key).items()]
            for name, regex_value in app.get(key).items():
                if name in meta:
                    content = meta.get(name)
                    if regex_value.search(content):
                        detected_app = True
        if key == "html":
            if isinstance(app.get(key), list):
                value_list = app.get(key)
                app.update({key: []})
                for html_value in value_list:
                    app.get(key).append(regex_pattern(html_value))
            else:
                value = app.get(key)
                app.update({key: [regex_pattern(value)]})
            for regex_value in app.get(key):
                if regex_value.search(content):
                    detected_app = True
        if key == "script":
            if isinstance(app.get(key), list):
                value_list = app.get(key)
                app.update({key: []})
                for script in value_list:
                    app.get(key).append(regex_pattern(script))
            else:
                value = app.get(key)
                app.update({key: [regex_pattern(value)]})
            for regex_value in app.get(key):
                    for script in content:
                        if regex_value.search(script):
                            detected_app = True

    return False
